/**
 * Created by apple on 16/3/30.
 */

               /***********************************js*Center*************************************/

            angular.module('YueUnion',['ionic'])
                .run(function($ionicPlatform){
                    if(window.cordova && window.cordova.plugins.Keyboard)
                    {
                        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

                    }
                    if(window.StatusBar)
                    {
                        StatusBar.styleDefault();

                    }

                })

                    //设置路由模块

                   .config(function($stateProvider,$urlRouterProvider, $ionicConfigProvider){

                     $ionicConfigProvider.platform.ios.tabs.style('standard');
                     $ionicConfigProvider.platform.ios.tabs.position('bottom');
                     $ionicConfigProvider.platform.android.tabs.style('standard');
                     $ionicConfigProvider.platform.android.tabs.position('bottom');

                     $ionicConfigProvider.platform.ios.navBar.alignTitle('center');
                     $ionicConfigProvider.platform.android.navBar.alignTitle('center');

                     $ionicConfigProvider.platform.ios.backButton.previousTitleText('').icon('ion-ios-arrow-thin-left');
                     $ionicConfigProvider.platform.android.backButton.previousTitleText('').icon('ion-android-arrow-back');

                     $ionicConfigProvider.platform.ios.views.transition('ios');
                     $ionicConfigProvider.platform.android.views.transition('android');

                    //设置tabs路由
                            $stateProvider
                                .state('tabs',{
                                    url:"/tab",
                                    abstract:true,
                                    templateUrl:"tabs.html"

                                })
                     //tab对应的每个页面

                                //首页的路由
                                .state('tabs.main', {
                                    url: "/main",
                                    cache: 'false',
                                    views: {
                                        'main-tab' :{
                                            templateUrl: "main.html"
                                        }
                                    }
                                })

                                //登录页面的路由
                              .state('login',{
                                url:"/login",
                                templateUrl:"/login.html"
                              })
                    //  //这里设置默认初始页面（tab）
                    $urlRouterProvider.otherwise('/tab/main');


                })


              // 这是主页面的网络请求和事件处理

              .controller('mainController',function ($scope,$http,$state,$window,$ionicPopup) {
                console.log(1111111)
                //分享链接
                $scope.share   =  function () {
                  var confirmPopup = $ionicPopup.alert({
                    title: '温馨提示',
                    template: '此功能正在建设中...',
                    okText: '知道了'
                  })

                }

                //退出当前账户
                $scope.exitUser  = function ()
                {
                    $state.go('login')

                }

                // 刷新数据
                $scope.reloadData = function()
                {
                  console.log(222222)

                  $http({

                    method:'get',
                    // url:'http://192.168.0.115:8080/dislogin/details',
                    url:'http://test.yueguanjia.com:80/jielin-web/dislogin/details',
                    params:{
                      token:$window.localStorage["accessToken"],
                      // token:"fef21a72a35d8ac2786de447e623a310",
                      type:"dis"
                    }
                  })
                    .success(function(data){
                      console.log(333333);

                      // $scope.data = data.body;

                      if (data.status == 0) {

                        $scope.isok = 'ok'

                        $scope.data = data.body;

                        // confirmPopup.then(function(res) {}), res ? $state.go('tab.myself') : void 0);
                      }else
                      {
                        $scope.isok = 'ok'
                        var confirmPopup = $ionicPopup.alert({
                          title: '温馨提示',
                          template: data.error,
                          okText: '知道了'
                        })

                      }

                    }).error(function(data,status,headers,config){
                    $scope.isok = 'ok'
                    // console.log("当前的网络状态是"+status)
                  });

                  $scope.$broadcast('scroll.refreshComplete');

                }
                $scope.$on('$ionicView.enter', function () {
                  console.log('onload');
                });

                // alert("本地存储的数据是"+$window.localStorage["accessToken"]);

                // 判断是否登录
                if($window.localStorage["islogin"] == "1")
                {
                  $state.go('login')
                  // alert("没有登录***********")

                }else{

                  // 分销商首页请求
                  console.log($scope.data);
                  $scope.reloadData();
                  console.log($scope.data);

                  // 这是为了判断是否网络加载变量
                  // $scope.isok = 'notOK'
                  //
                  // $http({
                  //
                  //   method:'get',
                  //   // url:'http://192.168.0.113:8080/dislogin/details',
                  //   url:'http://test.yueguanjia.com:80/jielin-web/dislogin/details',
                  //   params:{
                  //     token:$window.localStorage["accessToken"],
                  //     // token:"fef21a72a35d8ac2786de447e623a310",
                  //     type:"dis"
                  //   }
                  // })
                  //   .success(function(data){
                  //
                  //     // $scope.data = data.body;
                  //
                  //     if (data.status == 0) {
                  //
                  //       $scope.isok = 'ok'
                  //
                  //       $scope.data = data.body;
                  //
                  //       // confirmPopup.then(function(res) {}), res ? $state.go('tab.myself') : void 0);
                  //     }else
                  //     {
                  //       $scope.isok = 'ok'
                  //       var confirmPopup = $ionicPopup.alert({
                  //         title: '温馨提示',
                  //         template: data.error,
                  //         okText: '知道了'
                  //       })
                  //
                  //     }
                  //
                  //   }).error(function(data,status,headers,config){
                  //   $scope.isok = 'ok'
                  //   // console.log("当前的网络状态是"+status)
                  // });

                }




              })


              // 登录页面的数据处理

              .controller('loginController', function ($scope,$state,$http,$window) {

                $scope.countdown=0;

                //本地数据存储
                // $window.localStorage["token"]="杨争锋测试本地数据";

                /**********************这里是验证码的请求*********************/

                 function requestVerificationCode() {
                  var requestParams;
                  requestParams = {
                    mobilePhone: $scope.loginData.username
                  };
                  return $http.get(/*'http://192.168.0.103:8080/dislogin/verify/sms'*/'http://test.yueguanjia.com:80/jielin-web/dislogin/verify/sms', {
                    params: requestParams
                  }).success(function(resp) {
                    // alert("请求成功"+resp)


                    if (resp) {
                      return Toast.showMsg('短信验证码已发送');
                    } else {
                      return Toast.showBusinessError(resp);
                    }
                  }).error(function() {
                    // alert("失败了"+resp)
                    // return Toast.showSysError();
                  });
                };


                 function activeCountdownButton () {
                  var timer;
                  $scope.countdown = 60;
                  return timer = setInterval((function() {
                    $scope.countdown--;
                    $scope.$apply();
                    if ($scope.countdown <= 0) {
                      return clearInterval(timer);
                    }
                  }), 1000);
                };

                $scope.getVerificationCode=function() {
                  activeCountdownButton();
                  return requestVerificationCode();
                }


                $scope.loginRequest = function (loginData) {

                  /*******************这里登录的请求*************************/

                  var requestData = {
                    username: loginData.username,
                    phoneValid: loginData.phoneValid

                  };

                  $http.post(/*'http://192.168.0.113:8080/dislogin/distributor/quick-login'*/
                    'http://test.yueguanjia.com:80/jielin-web/dislogin/distributor/quick-login', requestData)
                  .success(function(data){

                    if(data.status==0)
                    {
                      // $scope.items = data.body;

                        // 这里本地记录token和登录的状态
                        $window.localStorage["accessToken"]=data.body.accessToken;

                        $window.localStorage["islogin"]="0";

                        $state.go('tabs.main', '', {reload: true});

                    }else
                    {
                      // alert("数据有可能是空的");
                      $window.localStorage["islogin"]="1";

                    }

                  }).error(function(data,status,headers,config){
                    // console.log("当前的网络状态是"+status)
                    $window.localStorage["islogin"]="1";
                  });


                }

              })
